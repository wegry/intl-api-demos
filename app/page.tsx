'use client'

import { useCallback, useEffect, useMemo, useRef, useState } from 'react'
import styles from './page.module.css'
import { intervalToDuration } from 'date-fns'
import { useRouter } from 'next/navigation'

const enum Param {
  Locales = 'locales',
  start = 'start',
  end = 'end',
}

export default function Home() {
  const [isClient, setIsClient] = useState(false)

  const router = useRouter()
  const [start, setStart] = useState<string | null>(null)
  const [rawLocales, setRawLocales] = useState<string>('')
  const [end, setEnd] = useState('1970-01-01')

  useEffect(() => {
    const params = new URLSearchParams(location.search)
    setStart(params.get(Param.start) ?? null)
    setEnd(params.get(Param.end) ?? new Date().toISOString())
    setRawLocales(
      params?.get(Param.Locales) ??
        'en-US, en-GB, en-CA, ja, zh, ko, th, km, vi, hi,  de-DE, ar-AE, he, fr-CA, es-US, es-ES, it, pl, hu, eu, el, uk, ru, ka, am'
    )
    setIsClient(true)
  }, [])

  useEffect(() => {
    if (!isClient) {
      return
    }
    const params = new URLSearchParams()
    if (start) {
      params.set(Param.start, start)
    }
    params.set(Param.end, end)

    params.set(
      Param.Locales,
      rawLocales
        .split(',')
        .flatMap((x) => {
          const trimmed = x.trim()
          return trimmed ? [trimmed] : []
        })
        .join(',')
    )

    router.replace(`?${params.toString()}`, { scroll: false })
  }, [rawLocales, end, isClient])

  const sortLocales = useCallback(() => {
    setRawLocales((rawLocales) => {
      return rawLocales
        .split(',')
        .flatMap((x) => {
          const trimmed = x.trim()

          return trimmed ? [trimmed] : []
        })
        .sort()
        .join(', ')
    })
  }, [rawLocales])
  const parsedStart = new Date(start ?? new Date())
  const parsedEnd = new Date(end)

  const rows = useMemo(() => {
    const duration = intervalToDuration({
      end: parsedEnd,
      start: parsedStart,
    })

    let biggestUnit: Intl.RelativeTimeFormatUnit = 'second'
    let value = 0

    for (const key of [
      'years',
      'months',
      'days',
      'hours',
      'minutes',
      'seconds',
    ] as const) {
      const val = duration[key]
      if (val) {
        if (key === 'days' && val >= 7) {
          biggestUnit = 'week'
          value = Math.round(val / 7)
          break
        }

        biggestUnit = key
        value = val
        break
      }
    }

    return rawLocales
      .split(',')
      .flatMap((x) => x.trim() || [])
      .map((locale, i) => {
        let defaultFormatterAuto
        try {
          defaultFormatterAuto = new Intl.RelativeTimeFormat(locale, {
            numeric: 'auto',
          })
        } catch (e) {
          if (e instanceof RangeError) {
            return (
              <tr key={i}>
                <td />
                <td>{locale}</td>
                <td colSpan={6}></td>
              </tr>
            )
          }
          throw e
        }
        const shortFormatterAuto = new Intl.RelativeTimeFormat(locale, {
          numeric: 'auto',
          style: 'short',
        })
        const narrowFormatterAuto = new Intl.RelativeTimeFormat(locale, {
          numeric: 'auto',
          style: 'narrow',
        })
        const defaultFormatterAlways = new Intl.RelativeTimeFormat(locale, {
          numeric: 'always',
        })

        const shortFormatterAlways = new Intl.RelativeTimeFormat(locale, {
          numeric: 'always',
          style: 'short',
        })

        const narrowFormatterAlways = new Intl.RelativeTimeFormat(locale, {
          numeric: 'always',
          style: 'narrow',
        })

        return (
          <tr key={locale + end}>
            <td>
              {new Intl.DisplayNames([], { type: 'language' }).of(locale)}{' '}
            </td>
            <td>
              <code>{locale}</code>
            </td>
            <td>{defaultFormatterAuto.format(value, biggestUnit)}</td>
            <td>{shortFormatterAuto.format(value, biggestUnit)}</td>
            <td>{narrowFormatterAuto.format(value, biggestUnit)}</td>
            <td>{defaultFormatterAlways.format(value, biggestUnit)}</td>
            <td>{shortFormatterAlways.format(value, biggestUnit)}</td>
            <td>{narrowFormatterAlways.format(value, biggestUnit)}</td>
          </tr>
        )
      })
  }, [parsedEnd, rawLocales, parsedStart])

  if (isClient === false) {
    return
  }

  return (
    <section className={styles.main}>
      <h1>
        Intl.RelativeTimeFormat <span className="noto-emoji">⏱️</span>
      </h1>
      <div className={styles.description}>
        <h2>Background</h2>
        <p>
          What does{' '}
          <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl/RelativeTimeFormat">
            <code>Intl.RelativeTimeFormat</code>
          </a>{' '}
          render for different inputs across locales? Given a{' '}
          <a href="https://www.w3.org/International/articles/language-tags/">
            list of language tags
          </a>
          , this page will render the output of{' '}
          <code>Intl.RelativeTimeFormat</code> <code>format</code> for each
          language tag provided.
        </p>
      </div>
      <div className={styles.controlsSection}>
        <h2>Controls</h2>
        <div className={styles.controls}>
          <label className={styles.locales}>
            <div>Locales (comma separated)</div>
            <input
              type="text"
              style={{
                width: '100%',
              }}
              onChange={(e) => {
                setRawLocales(e.target.value)
              }}
              value={rawLocales}
            />
          </label>
          <label>
            <div>Start Date</div>
            <input
              type="datetime-local"
              onChange={(e) => {
                setStart(e.target.value)
              }}
              value={(start ?? new Date().toISOString()).replace(/Z$/, '')}
            />
            <button
              onClick={() => {
                setEnd(start ?? new Date().toISOString())
              }}
            >
              Now
            </button>
          </label>
          <label>
            <div>End Date</div>
            <input
              type="datetime-local"
              onChange={(e) => {
                setEnd(e.target.value)
              }}
              value={end?.replace(/Z$/, '')}
            />
            <button
              onClick={() => {
                setEnd(start ?? new Date().toISOString())
              }}
            >
              Reset
            </button>
          </label>

          {/* <dl>
            <dt>Start</dt>
            <dd>
              <code>{parsedStart.toISOString()}</code>
            </dd>
            <dt>End</dt>
            <dd>
              <code>{new Date(end).toISOString()}</code>
            </dd>
          </dl> */}
        </div>
      </div>
      <div className={styles.results}>
        <h2>
          Output&nbsp;&nbsp;
          <code>
            Start: {parsedStart.toISOString().replace(/Z$/, '')} / End{' '}
            {parsedEnd.toISOString().replace(/Z$/, '')}{' '}
          </code>
        </h2>
        <table>
          <colgroup>
            <col span={3} className={styles.languageTag} />
            <col span={3} className={styles.numericAuto} />
            <col span={3} className={styles.numericAlways} />
          </colgroup>
          <thead>
            <tr>
              <th colSpan={2} onClick={sortLocales}>
                Language tag
              </th>
              <th colSpan={3}>
                <code>numeric: auto</code>
              </th>
              <th colSpan={3}>
                <code>numeric: always</code>
              </th>
            </tr>
            <tr>
              <th onClick={sortLocales}>Name</th>
              <th onClick={sortLocales}>Code</th>
              {Array.from(Array(2), (x) => null).flatMap((_, i) =>
                ['default', 'short', 'narrow'].map((x, j) => (
                  <th key={`${i} ${j}`}>
                    <code>{x}</code>
                  </th>
                ))
              )}
            </tr>
          </thead>
          <tbody>{rows}</tbody>
        </table>
      </div>
    </section>
  )
}
