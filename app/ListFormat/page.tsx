'use client'

import { useEffect, useMemo, useState } from 'react'

const locales = [
  'ar',
  'da-DK',
  'de-DE',
  'en-CA',
  'en-US',
  'el',
  'es-US',
  'es',
  'et-EE',
  'fi-FI',
  'fr-CA',
  'hi-HI',
  'hu',
  'ja',
  'km',
  'ko',
  'lt-LT',
  'lv-LV',
  'pl',
  'pt-BR',
  'pt-PT',
  'se-SE',
  'th',
  'ru',
  'uk',
  'zh-SG',
  'zh-US',
]

const list = ['a', 'b', 'c']

export default function ListFormat() {
  const [isClient, setIsClient] = useState(false)
  useEffect(() => {
    setIsClient(true)
  }, [])
  const languages = globalThis.navigator?.languages ?? []
  const LanguageTagName = useMemo(() => {
    return new Intl.DisplayNames(languages, {
      type: 'language',
    })
  }, [languages])

  if (!isClient) {
    return null
  }

  return (
    <section>
      <h1>
        Intl.ListFormat <span className="noto-emoji">✅</span>
      </h1>
      <table>
        <thead>
          <tr>
            <th>Language Tag</th>
            <th>Locale name</th>
            <th>Formatted List</th>
          </tr>
        </thead>
        <tbody>
          {locales.map((locale) => (
            <tr key={locale}>
              <td>{locale}</td>
              <td>{LanguageTagName.of(locale)} </td>
              <td>{new Intl.ListFormat(locale).format(list)}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </section>
  )
}
