import type { Metadata } from 'next'
import Link from 'next/link'
import './globals.css'

export const metadata: Metadata = {
  title: 'Intl in Various Locales',
  description:
    'See how the ECMAScript Internationalization API works in various locales',
}

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode
}>) {
  return (
    <html lang="en">
      <head>
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link
          href="https://fonts.googleapis.com/css2?family=Noto+Emoji:wght@300..700&display=swap"
          rel="stylesheet"
        />
      </head>
      <body>
        <header>
          <h1>Intl in Various Locales</h1>
        </header>
        <aside>
          <h2>Pages</h2>
          <ul>
            <li>
              <Link href="/ListFormat">ListFormat</Link>
            </li>
            <li>
              <Link href="/">RelativeTimeFormat</Link>
            </li>
          </ul>
        </aside>
        <main>{children}</main>
      </body>
    </html>
  )
}
